#
# Module: essioc
#
require essioc

#
# Module: lm510
#
require lm510,1.1.0


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${lm510_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: Spk-000CDL:Cryo-LC-001
# Module: lm510
#
iocshLoad("$(lm510_DIR)/lm510.iocsh", "DEVICENAME = Spk-000CDL:Cryo-LT-91, IPADDR = cds-spk-000-level-ctrl.tn.esss.lu.se")

#
# Device: Spk-000CDL:Cryo-LT-91
# Module: lm510
#
iocshLoad("$(lm510_DIR)/lm510_lt.iocsh", " DEVICENAME = Spk-000CDL:Cryo-LC-001, CONTROLLER = Spk-000CDL:Cryo-LC-001, CHANNEL = 1")

#
# Device: Spk-000CDL:Cryo-LT-92
# Module: lm510
#
#iocshLoad("$(lm510_DIR)/lm510_lt.iocsh", " DEVICENAME = Spk-000CDL:Cryo-LT-92, CONTROLLER = Spk-000CDL:Cryo-LC-001, CHANNEL = 2")
